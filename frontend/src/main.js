import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import VueAudio from 'vue-audio-better'
import VueRouter from 'vue-router'
import VueMeta from 'vue-meta'

Vue.use(VueRouter)
Vue.use(VueAudio)
Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
})

Vue.config.productionTip = false

import Index from './components/Index.vue'
import Party from './components/Party.vue'
import JoinParty from './components/JoinParty.vue'

const router = new VueRouter({
  routes: [
    { path: '/', component: Index, name: 'index'},
    { path: '/party/:uid/join', component: JoinParty,  name: 'join_party' },
    { path: '/party/:uid', component: Party,  name: 'party' },
  ],
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
