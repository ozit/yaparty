updatefront:
	git pull origin master && docker-compose -f docker-compose.prod.yml build frontend && docker-compose -f docker-compose.prod.yml stop && docker-compose -f docker-compose.prod.yml up -d
updateback:
	git pull origin master && docker-compose -f docker-compose.prod.yml build app && docker-compose -f docker-compose.prod.yml stop && docker-compose -f docker-compose.prod.yml up -d
update:
	git pull origin master && docker-compose -f docker-compose.prod.yml build app frontend && docker-compose -f docker-compose.prod.yml stop && docker-compose -f docker-compose.prod.yml up -d
