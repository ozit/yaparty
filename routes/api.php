<?php

use App\Http\Controllers\PartyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/party/{partyUid}/playlist', [PartyController::class, 'getPlaylist'])->name('get_playlist');

Route::post('/v1/party/{partyUid}/playlist', [PartyController::class, 'generatePlaylist'])->name('generate_playlist');

Route::post('/v1/party/{partyUid}/join', [PartyController::class, 'join'])->name('join_party');

Route::post('/v1/party', [PartyController::class, 'create'])->name('create_party');

Route::get('/v1/party/{partyUid}', [PartyController::class, 'show'])->name('show_party');

Route::get('/v1/track/{trackId}', [PartyController::class, 'getTrackUrl'])->name('get_track_url');
