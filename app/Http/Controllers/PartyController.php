<?php


namespace App\Http\Controllers;


use App\Models\Member;
use App\Models\Party;
use App\Services\PlaylistService;
use App\Services\UserService;
use Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PartyController extends Controller
{
    private UserService     $userService;
    private PlaylistService $playlistService;

    public function __construct(UserService $userService,
                                PlaylistService $playlistService)
    {
        $this->userService     = $userService;
        $this->playlistService = $playlistService;
    }

    public function join(Request $request, string $partyUid): JsonResponse
    {
        $login = str_replace("@yandex.ru", "", $request->get('login'));
        $party = Party::where('uid', $partyUid)->first();

        if ($party === null || $login === null) {
            return new JsonResponse(['message' => 'missed parameters'], 400);
        }

        $userInfo = retry(5, fn() => $this->userService->getInfo($login), 200);

        if (empty($userInfo)) {
            return new JsonResponse(['message' => 'user not found'], 404);
        }

        $member = Member::where('party_uid', $partyUid)->where('login', $login)->first();

        if ($member !== null) {
            return new JsonResponse();
        }

        $member            = new Member();
        $member->party_uid = $partyUid;
        $member->login     = $login;
        $member->user_info = $userInfo;
        $member->save();

        return new JsonResponse();
    }

    public function create(Request $request): JsonResponse
    {
        $partyName = $request->get('partyName');
        $login     = str_replace("@yandex.ru", "", $request->get('login'));

        if ($partyName === null || $login === null) {
            return new JsonResponse(['message' => 'missed parameters'], 400);
        }

        $userInfo = retry(5, fn() => $this->userService->getInfo($login), 200);

        if (empty($userInfo)) {
            return new JsonResponse(['message' => 'user not found'], 404);
        }

        $playlist = $this->playlistService->generate([$login], '');

        if (empty($playlist)) {
            $playlist = $this->playlistService->generate([$login], 'preset1');
        }

        $party           = new Party();
        $party->uid      = Str::uuid()->toString();
        $party->name     = $partyName;
        $party->owner    = $login;
        $party->playlist = $playlist;
        $party->save();

        $member            = new Member();
        $member->party_uid = $party->uid;
        $member->login     = $login;
        $member->user_info = $userInfo;
        $member->save();

        $joinUrl = sprintf(
            "%s://%s/party/%s/join",
            $request->getScheme(),
            $request->getHost(),
            $party->uid,
        );

        return new JsonResponse([
            'joinUrl' => $joinUrl,
            'uid'     => $party->uid,
        ]);
    }

    public function show(Request $request, string $partyUid): JsonResponse
    {
        if (!Uuid::isValid($partyUid)) {
            return new JsonResponse(['message' => 'party not found'], 404);
        }

        $party = Party::where('uid', $partyUid)->first();

        if ($party === null) {
            return new JsonResponse(['message' => 'party not found'], 404);
        }

        $members = Member::where('party_uid', $partyUid)->get();

        $users = [];
        foreach ($members as $member) {
            $users[] = $member->user_info;
        }

        $joinUrl = sprintf(
            "%s://%s/party/%s/join",
            $request->getScheme(),
            $request->getHost(),
            $party->uid,
        );

        return new JsonResponse([
            'partyName' => $party->name,
            'partyOwner' => $party->owner,
            'users'     => $users,
            'joinUrl'   => $joinUrl,
            'createdAt' => $party->created_at->timestamp
        ]);
    }

    public function getPlaylist(string $partyUid): JsonResponse
    {
        $party = Party::where('uid', $partyUid)->first();

        if ($party === null) {
            return new JsonResponse(['message' => 'party not found'], 404);
        }

        return new JsonResponse($party->playlist);
    }

    public function generatePlaylist(Request $request, string $partyUid): JsonResponse
    {
        $type  = $request->get('type', '');
        $party = Party::where('uid', $partyUid)->first();

        if ($party === null) {
            return new JsonResponse(['message' => 'party not found'], 404);
        }

        $members = Member::where('party_uid', $partyUid)->get();

        $logins = [];
        foreach ($members as $member) {
            $logins[] = $member->login;
        }

        $playlist = $this->playlistService->generate($logins, $type);

        if (empty($playlist)) {
            $playlist = $this->playlistService->generate($logins, 'preset1');
        }

        $party->playlist = $playlist;
        $party->save();

        return new JsonResponse();
    }

    public function getTrackUrl(int $trackId): JsonResponse
    {
        $trackUrl = '';

        try {
            $trackUrl = retry(2, fn() => $this->playlistService->getTrackUrl($trackId, true), 200);
        } catch (Exception $exception) {
            try {
                $trackUrl = retry(3, fn() => $this->playlistService->getTrackUrl($trackId), 200);
            } catch (Exception $exception) {
                return new JsonResponse(['message' => 'track mp3 url not found'], 404);
            }
        }

        return new JsonResponse(['trackUrl' => $trackUrl]);
    }
}
