<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class PlaylistService
{
    private const FUNCTION_GET_MP3_LINK          = 'https://functions.yandexcloud.net/d4efkqtji37andhrmrof?track-id=';
    private const FUNCTION_GET_USER_LIKED_TRACKS = 'https://functions.yandexcloud.net/d4efucp12trmts1uv0mb?login=';
    private const FUNCTION_GET_TRACKS_INFO       = 'https://functions.yandexcloud.net/d4e9o2m9qrr4g38krdma?tracks=';
    private const FUNCTION_GET_PRESET_1          = 'https://functions.yandexcloud.net/d4erdv4af0b5l6d6u4nq';

    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function generate(array $logins, string $type): array
    {
        $usersLikesTracks = [];
        foreach ($logins as $login) {
            $tracks = Cache::remember("get_liked_tracks:{$login}", 60 * 10, function() use ($login) {
               return retry(5, fn() => $this->getUserLikedTracks($login), 300);
            });
            shuffle($tracks);
            $usersLikesTracks[$login] = $tracks;
        }

        $playlistTracks = [];
        switch ($type) {
            case 'preset1':
                $playlistTracks = $this->getPreset1();
                break;
            default:
                $playlistTracks = $this->getLikedTrackList($usersLikesTracks);
                break;
        }

        $playlist = retry(3, fn() => $this->getTracksInfo($playlistTracks), 300);
        foreach ($playlist as &$track) {
            $track['liked'] = [];
            foreach ($usersLikesTracks as $login => $likedTracks) {
                if (in_array($track['id'], $likedTracks)) {
                    $track['liked'][] = $login;
                }
            }
        }

        return $playlist;
    }

    public function getTracksInfo(array $trackIds): array
    {
        $tracks = implode(',', $trackIds);
        $link   = self::FUNCTION_GET_TRACKS_INFO . $tracks;

        $tracksInfo = $this->sendApiRequest($link);

        return $tracksInfo ? $tracksInfo['tracks'] : [];
    }

    public function getTrackUrl(int $trackId, bool $full = false): string
    {
        $link = self::FUNCTION_GET_MP3_LINK . $trackId;
        if ($full) {
            $link .= '&auth';
        }

        $trackUrl = Cache::remember($link, 60 * 10, function() use ($link) {
            return $this->sendApiRequest($link);
        });

        return $trackUrl ? $trackUrl['url'] : '';
    }

    public function getUserLikedTracks(string $login): array
    {
        $link        = self::FUNCTION_GET_USER_LIKED_TRACKS . $login;
        $likedTracks = $this->sendApiRequest($link);

        return $likedTracks ?? [];
    }

    private function getLikedTrackList(array $usersLikesTracks): array
    {
        $allLikedTracks = [];
        foreach ($usersLikesTracks as $likedTracks) {
            $allLikedTracks = array_merge($allLikedTracks, $likedTracks);
        }

        $likedCounts = [];
        foreach ($allLikedTracks as $likedTrack) {
            if (!isset($likedCounts[$likedTrack])) {
                $likedCounts[$likedTrack] = 1;
                continue;
            }

            $likedCounts[$likedTrack]++;
        }
        arsort($likedCounts);

        $playlistTracks = [];

        foreach ($likedCounts as $trackId => $likesCount) {
            if ($likesCount === 1) {
                break;
            }

            if (count($playlistTracks) >= 100) {
                break;
            }

            $playlistTracks[] = (string)$trackId;
            unset($likedCounts[$trackId]);
        }

        $tempUsersLikesTracks = $usersLikesTracks;
        while (count($likedCounts) > 0 && count($playlistTracks) < 100) {
            foreach ($tempUsersLikesTracks as &$likedTracks) {
                $trackId = array_shift($likedTracks);

                if ($trackId === null) {
                    continue;
                }

                if (isset($likedCounts[$trackId]) && !in_array($trackId, $playlistTracks)) {
                    $playlistTracks[] = $trackId;
                }

                if (isset($likedCounts[$trackId])) {
                    unset($likedCounts[$trackId]);
                }
            }
        }

        return $playlistTracks;
    }

    private function getPreset1(): array
    {
        $presetTracks = retry(3, fn() => $this->sendApiRequest(self::FUNCTION_GET_PRESET_1), 300);
        $presetTracks = $presetTracks ? $presetTracks['tracks'] : [];

        if (count($presetTracks) > 100) {
            $presetTracks = array_slice($presetTracks, 0, 100);
        }

        return $presetTracks;
    }

    private function sendApiRequest(string $url): ?array
    {
        $result = null;

        try {
            $response = $this->client->request('GET', $url);
            $body     = $response->getBody()->getContents();
            $result   = json_decode($body, true);
        } catch (GuzzleException $e) {
            //todo for logging
            throw $e;
        }

        return $result;
    }
}
