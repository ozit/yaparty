<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class UserService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getInfo(string $login): array
    {
        $link = 'https://functions.yandexcloud.net/d4eqdrod603s9fem3uc4?login=' . $login;

        $info = [];
        try {
            $response    = $this->client->request('GET', $link);
            $body        = $response->getBody()->getContents();
            $decodedBody = json_decode($body, true);
            return $decodedBody ? $decodedBody['user'] : [];
        } catch (GuzzleException $e) {
            throw $e;
        }
    }
}
